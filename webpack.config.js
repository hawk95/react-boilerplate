var path = require('path');
var webpack = require( 'webpack');
var debug = process.env.NODE_ENV !== 'production';

module.exports = {
	entry: './src/index.js',
	output: {
		filename: 'boundle.js',
		path: path.resolve( __dirname, 'dist' )
	},
	devtool: debug ? "inline-sourcemap" : null,
	devServer: {
		contentBase: path.resolve(__dirname, 'dist'),
		compress: true,
		port: 9000
	},
	module: {
		loaders:
		[
			{
				test: /\.jsx?$/,
				loader: 'babel-loader',
				exclude: '/node_modules/',
				query: {
					presets: ['react', 'es2015']
				}
			}
		]
	},

	plugins: debug ? [] : [
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
	]
}